/*
 * @file inlines.h
 * @brief Contains inline utililty functions whose visibility is needed by more
 *        than one translation unit.
 *
 * @date 27 Mar 2012
 * @author abe
 */

#ifndef INLINES_H_
#define INLINES_H_
#ifdef __cplusplus
extern "C" {
#endif

#include "macros.h"

LOCAL INLINE STATUS write_1byte(RAWBUF_ptr buf, const UINT32 val) {
    if(!buf->free)
        error_exit("write_1byte()", FAILURE, MSG_OUTOFBOUNDS);
    buf->data[buf->pos++] = val & 0xFF;
    buf->free--;
    RETURN_STATUS(SUCCESS);
}

LOCAL INLINE STATUS write_2bytes(RAWBUF_ptr buf, const UINT32 val) {
    if(buf->free < 2)
        error_exit("write_2bytes()", FAILURE, MSG_OUTOFBOUNDS);
    buf->data[buf->pos++] = (val >> 8) & 0xFF;
    buf->data[buf->pos++] = val & 0xFF;
    buf->free -= 2;
    RETURN_STATUS(SUCCESS);
}

LOCAL INLINE STATUS write_nbytes(RAWBUF_ptr buf, const CHAR *val, UINT32 count) {
    if(buf->free < count)
        error_exit("write_nbytes()", FAILURE, MSG_OUTOFBOUNDS);
    while(count--) {
        write_1byte(buf, (*val++ & 0xFF));
    }
    RETURN_STATUS(SUCCESS);
}

#ifdef __cplusplus
}
#endif
#endif  /* INLINES_H_ */

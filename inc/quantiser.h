/**
 * @file quantiser.h
 *
 * @date 10 Jan 2012
 * @author abe
 */

#ifndef QUANTISER_H_
#define QUANTISER_H_
#ifdef __cplusplus
extern "C" {
#endif

#include "macros.h"

#define LUMA   (0)
#define CHROMA (1)


/**
 * Luminance quantisation table
 */
static const UINT8 QTABLE_Y[MCU_SQUARE] = {
    16,  11,  10,  16,  24,  40,  51,  61,
    12,  12,  14,  19,  26,  58,  60,  55,
    14,  13,  16,  24,  40,  57,  69,  56,
    14,  17,  22,  29,  51,  87,  80,  62,
    18,  22,  37,  56,  68, 109, 103,  77,
    24,  36,  55,  64,  81, 104, 113,  92,
    49,  64,  78,  87, 103, 121, 120, 101,
    72,  92,  95,  98, 112, 100, 103,  99
};

/**
 * Chrominance quantisation table
 */
static const UINT8 QTABLE_UV[MCU_SQUARE] = {
    17, 18, 24, 47, 99, 99, 99, 99,
    18, 21, 26, 66, 99, 99, 99, 99,
    24, 26, 56, 99, 99, 99, 99, 99,
    47, 66, 99, 99, 99, 99, 99, 99,
    99, 99, 99, 99, 99, 99, 99, 99,
    99, 99, 99, 99, 99, 99, 99, 99,
    99, 99, 99, 99, 99, 99, 99, 99,
    99, 99, 99, 99, 99, 99, 99, 99
};

struct tagQUANT_TBL {
    UINT8 qcoeff[64];
};

/**
 * Functions
 */
/* Public */
STATUS init_quantiser(JPEG_ENC_ptr cjpg);
STATUS terminate_quantiser(JPEG_ENC_ptr cjpg);
/* Internal */
STATUS create_quant_tables(QUANT_TBL_ptr *qtbls, const UINT8 *luma, const UINT8 *chroma, const UINT32 quality, BOOL is_colour);
STATUS destroy_quant_tables(QUANT_TBL_ptr *qtbls, BOOL is_colour);
STATUS mcu_block_fdct(JPEG_ENC_ptr cjpg, const INT32 *in, INT32 *out);
STATUS mcu_block_quantise(JPEG_ENC_ptr cjpg, const QUANT_TBL_ptr qtbl, const INT32 *in, INT32 *out);

#ifdef __cplusplus
}
#endif
#endif /* QUANTISER_H */

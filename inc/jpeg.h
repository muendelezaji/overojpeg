/**
 * @file jpeg.h
 *
 * @date 10 Jan 2012
 * @author abe
 */

#ifndef JPEG_H_
#define JPEG_H_
#ifdef __cplusplus
extern "C" {
#endif

#include "macros.h"

/**
 * Maximum size of JPEG width or height supported by the standard (in pixels)
 */
#define JPEG_MAX_DIMENSION   65535L

/**
 * JPEG markers
 */
typedef enum tagJPEG_MARKER {
    MK_SOF0     = 0xC0,
    MK_SOF1     = 0xC1, // Go up to SOF15=0xCF, except 0xC4 and 0xCC
    MK_DHT      = 0xC4,
    MK_RST0     = 0xD0, // Go up to RST7=0xD7
    MK_SOI      = 0xD8,
    MK_EOI      = 0xD9,
    MK_SOS      = 0xDA,
    MK_DQT      = 0xDB,
    MK_DNL      = 0xDC, // Number of lines
    MK_DRI      = 0xDD,
    MK_APP0     = 0xE0,
    MK_APP1     = 0xE1, // Go up to APP15=0xEF
    MK_COM      = 0xFE
} JPEG_MARKER;

/**
 * Colour space
 */
typedef enum tagCOLOUR_SPACE {
    CS_UNKNOWN,
    CS_GRAY,
    CS_RGB,
    CS_YUV
} COLOUR_SPACE;

/**
 * Downsampling target format
 */
typedef enum tagSAMPLE_FORMAT {
    SF_NONE,
    SF_422,
    SF_420,
    SF_411
} SAMPLE_FORMAT;

typedef UINT8 JPEG_SAMPLE, *JPEG_SAMPLE_ptr, *JPEG_SCAN, **JPEG_SCAN_ptr;

/**
 * JPEG encoder object
 */
struct tagJPEG_ENC {
    /* Input parameters */
    UINT32 width, height;   // Input image width and height
    COLOUR_SPACE cs;		// Colour space of input stream
    SAMPLE_FORMAT sf;		// Format to use when downsampling
    UINT32 components;      // Number of input colour components (max. 3)
    UINT32 precision;       // Bits per sample
    UINT32 quality;         // Linear scaling percentage of quantisation tables (100 = none)
    /* Working state and output params */
    BOOL is_colour;         // May be implied by cs
    UINT32 mcu_count;       // Number of Minimum Coded Units (integral)
    RAWBUF_ptr src;         // Raw pixel data to be compressed
    RAWBUF_ptr dst;         // Destination for compressed data
    JPEG_SAMPLE_ptr scans[3]; // Image data in 3D array (width x height x components)
    COLOUR_TBL_ptr cctbl;   // RGB to YCbCr colour conversion table
    QUANT_TBL_ptr qtbls[2]; // Quantisation tables for an encoder instance
    HUFF_TBL_ptr htbls[4];  // Derived Huffman tables
    HSPEC_TBL_ptr hspec[4]; // Specification Huffman tables
};

/**
 * External applications should fill this in and use it to pass 'per-image'
 * parameters
 */
typedef struct tagJPEG_PARAMS {
    INT32 width;                // Input image width
    INT32 height;               // Input image height
    INT32 components;           // Number of components in input
    UINT32 quality;             // Output JPEG quality
    COLOUR_SPACE colourspace;   // Input colourspace
    SAMPLE_FORMAT sampleformat; // Desired downsampling format
    BOOL extern_buffer;         // Buffer is alloc'd/freed by caller (avoids double alloc/free)
} JPEG_PARAMS_obj, *JPEG_PARAMS_ptr;

/**
 * Functions
 */
/* Public */
STATUS jpeg_compress(UINT8 *indata, JPEG_PARAMS_obj *params, JPEG_ENC_obj *cjpg, CHAR *filename, BOOL extern_init);
STATUS jpeg_init_compressor(JPEG_ENC_ptr cjpg);
STATUS jpeg_terminate_compressor(JPEG_ENC_ptr cjpg);
STATUS jpeg_write_scans(JPEG_ENC_ptr cjpg);
/* Internal */
LOCAL STATUS write_headers(JPEG_ENC_ptr cjpg);
LOCAL STATUS write_quant_tables(JPEG_ENC_ptr cjpg);
LOCAL STATUS write_huff_tables(JPEG_ENC_ptr cjpg);
/* Inline */
LOCAL INLINE STATUS write_marker(JPEG_ENC_ptr cjpg, JPEG_MARKER marker);
LOCAL INLINE STATUS write_jfif_header(JPEG_ENC_ptr cjpg);
LOCAL INLINE STATUS write_comment(JPEG_ENC_ptr cjpg, const CHAR *comment, const UINT32 length);
LOCAL INLINE STATUS write_frame_header(JPEG_ENC_ptr cjpg);
LOCAL INLINE STATUS write_scan_header(JPEG_ENC_ptr cjpg);

#ifdef __cplusplus
}
#endif
#endif /* JPEG_H_ */

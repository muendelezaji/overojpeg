/**
 * @file macros.h
 *
 * @date 10 Jan 2012
 * @author abe
 */

#ifndef MACROS_H_
#define MACROS_H_
#ifdef __cplusplus
extern "C" {
#endif

/**
 * Standard headers for integer and boolean types may be included if more types are needed
 */
#include <stdint.h>
#include <stdbool.h>

/**
 * Standard integer types defined here to avoid including the full stdint.h
 */
#ifndef _STDINT_H_
typedef signed char        INT8;
typedef unsigned char      UINT8;
typedef short              INT16;
typedef unsigned short     UINT16;
typedef int                INT32;
typedef unsigned int       UINT32;
typedef long long          INT64;
typedef unsigned long long UINT64;
#else
typedef int8_t             INT8;
typedef uint8_t            UINT8;
typedef int16_t            INT16;
typedef uint16_t           UINT16;
typedef int32_t            INT32;
typedef uint32_t           UINT32;
typedef int64_t            INT64;
typedef uint64_t           UINT64;
#endif

/**
 * Other types - allow changing underlying type without modifying the code
 */
typedef void               VOID;
typedef char               CHAR;
typedef unsigned char      UCHAR;
typedef float              FLOAT;
typedef double             DOUBLE;

#ifdef _SIZE_T
typedef size_t SIZE_T;
#else
typedef UINT32 SIZE_T;
#endif

/**
 * Boolean types for C
 */
#ifndef _STDBOOL
typedef unsigned int BOOL;
#ifndef FALSE
#define FALSE        (0)
#endif
#ifndef TRUE
#define TRUE         (1)
#endif
#else
typedef bool         BOOL;
#ifndef FALSE
#define FALSE        (false)
#endif
#ifndef TRUE
#define TRUE         (true)
#endif
#endif

/**
 * Function inlining
 */
#ifndef INLINE
#define INLINE __inline
#endif
#ifndef LOCAL
#define LOCAL static
#endif
#ifndef GLOBAL
#define GLOBAL
#endif

/**
 * Useful general macros
 */
#ifndef NULL
#define NULL         (0)
#endif

#ifndef SUCCESS
#ifndef EXIT_SUCCESS
#define SUCCESS      (0)
#else
#define SUCCESS      (EXIT_SUCCESS)
#endif
#endif

#ifndef FAILURE
#ifndef EXIT_FAILURE
#define FAILURE      (-1)
#else
#define FAILURE      (EXIT_FAILURE)
#endif
#endif

#ifndef SUCCEEDED
#define SUCCEEDED(x) ((x) == SUCCESS)
#endif

#ifndef FAILED
#define FAILED(x)    ((x) == FAILURE)
#endif

#ifndef SAFE_FREE
#define SAFE_FREE(x) do { if ((x) != NULL) { free(x); x = NULL; } } while(0)
#endif

/**
 * Function return status - if no status (void) return is desired, define
 * RETURN_VOID somewhere in the Makefile/build environment
 */
#ifndef RETURN_VOID
typedef INT32           STATUS; // Function return status codes
#define RETURN_STATUS(x)  return x
#else
typedef VOID            STATUS;
#define RETURN_STATUS(x)
#endif

/**
 * Error message and exit macros
 */
#define ERR_EXIT(x)      (fprintf(stderr, "ERROR: %s\n", x), exit(FAILURE))
#define MSG_OUTOFBOUNDS  "Cannot access beyond end of data."
#define MSG_MEMALLOC     "Failed to allocate memory."
#define MSG_FILEOPEN     "Failed to open file."
#define MSG_FILEREAD     "Failed to read file."
#define MSG_INVALIDHUFF  "Invalid Huffman code given."

/**
 * Common mathematical operations
 */
#ifndef MAX
#define MAX(a, b)    (((a) > (b))? (a) : (b))
#endif

#ifndef MIN
#define MIN(a, b)    (((a) < (b))? (a) : (b))
#endif

#ifndef ABS
#define ABS(x)       (((x) < 0)? -(x) : (x))
#endif

#ifndef ROUND
#define ROUND(x)     (((x) >= 0)? (INT32)((x) + 0.5) : (INT32)((x) - 0.5))
#endif

#ifndef CLAMP
#define CLAMP(x, low, high) (((x) > (high))? (high) : (((x) < (low))? (low) : (x)))
#endif

#ifndef CLAMP_MAX
#define CLAMP_MAX(x, high)  (((x) > (high))? (high) : (x))
#endif

#ifndef CLAMP_MIN
#define CLAMP_MIN(x, low)   (((x) < (low))? (low) : (x))
#endif

#ifndef EXPAND_XMODN
#define EXPAND_XMODN(x, n)  ((n) + (n) * ((x) / (n)))
#endif

/**
 * Useful mathematical constants (correct to 20 decimal places)
 */
#ifndef M_PI
#define M_PI         (3.14159265358979323846F)
#endif

#ifndef M_PI1_8
#define M_PI1_8      (0.39269908169872415481F)
#endif

#ifndef M_PI1_16
#define M_PI1_16     (0.19634954084936207740F)
#endif

#ifndef M_SQRT2
#define M_SQRT2      (1.41421356237309504880F)
#endif

#ifndef M_SQRT1_2
#define M_SQRT1_2    (0.70710678118654752440F)
#endif

#ifndef M_SQRT1_8
#define M_SQRT1_8    (0.35355339059327376220F)
#endif

/**
 * JPEG specific macros and forward declarations
 */
#define MCU_SIZE     (8)    // Size of one row of DCT coefficients
#define MCU_SQUARE   (64)   // Size of a block of DCT coefficients

typedef INT32 COLOUR_TBL_obj, *COLOUR_TBL_ptr;
typedef struct tagQUANT_TBL QUANT_TBL_obj, *QUANT_TBL_ptr;
typedef struct tagHUFF_TBL HUFF_TBL_obj, *HUFF_TBL_ptr;
typedef struct tagHSPEC_TBL HSPEC_TBL_obj, *HSPEC_TBL_ptr;
typedef struct tagRAWBUF RAWBUF_obj, *RAWBUF_ptr;
typedef struct tagJPEG_ENC JPEG_ENC_obj, *JPEG_ENC_ptr;

#ifdef __cplusplus
}
#endif
#endif /* MACROS_H_ */

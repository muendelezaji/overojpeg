/**
 * @file converter.h
 *
 * @date 10 Jan 2012
 * @author abe
 */

#ifndef PIXEL_H_
#define PIXEL_H_
#ifdef __cplusplus
extern "C" {
#endif

#include "macros.h"

/**
 * Offsets for colour conversion table - avoids the need store multiple tables
 * Total element count: 2048 UINT32s
 */
#define MAX_COLOR   (256)
#define Y_R         (0)
#define Y_G         (1 * MAX_COLOR)
#define Y_B         (2 * MAX_COLOR)
#define CB_R        (3 * MAX_COLOR)
#define CB_G        (4 * MAX_COLOR)
#define CB_B        (5 * MAX_COLOR)
#define CR_R        (CB_B)           // Same multiple as R_Cb
#define CR_G        (6 * MAX_COLOR)
#define CR_B        (7 * MAX_COLOR)
#define CCTBL_SIZE  (8 * MAX_COLOR)

/**
 * Fixed point to floating point operations
 */
#define FIXBITS     (16)
#define FIXHALF     ((INT32)(1L << (FIXBITS - 1)))
#define FIX128      ((INT32)(128 * (1L << FIXBITS)))
#define FIXEDPT(x)  ((INT32)((x) * (1L << FIXBITS) + 0.5))
#define UNFIXED(x)  ((INT32)((x) >> FIXBITS))

/**
 * Colour scan offsets
 */
#define RGB_R           (0)
#define RGB_G           (1)
#define RGB_B           (2)
#define YUV_Y           (0)
#define YUV_U           (1)
#define YUV_V           (2)

/**
 * Functions
 */
/* Public */
STATUS init_converter(JPEG_ENC_ptr cjpg);
STATUS terminate_converter(JPEG_ENC_ptr cjpg);
STATUS jpeg_preprocess(JPEG_ENC_ptr cjpg);
/* Internal */
STATUS create_rgb2yuv_table(COLOUR_TBL_ptr *ctbl);
STATUS destroy_rgb2yuv_table(COLOUR_TBL_ptr cctbl);
STATUS downsample(INT32 format);

#ifdef __cplusplus
}
#endif
#endif /* PIXEL_H_ */

/**
 * @file utilities.h
 *
 * @date 11 Jan 2012
 * @author abe
 */

#ifndef UTILITIES_H_
#define UTILITIES_H_
#ifdef __cplusplus
extern "C" {
#endif

#include "macros.h"

struct tagRAWBUF {
    SIZE_T size;        // Total I/O buffer size
    SIZE_T free;        // Free size left in buffer
    UINT32 pos;         // Position of read/write pointer in stream
    UINT32 bits_left;   // Number of bits left (to write out)
    UINT32 bit_accum;   // Accumulator for bits to be written out
    UINT8 *data;        // Buffer to hold raw data bytes
};

/**
 * Functions
 */
VOID error_exit(const CHAR *function, INT32 code, const CHAR *message);
VOID *mem_request(SIZE_T size);
VOID mem_release(VOID *ptr, SIZE_T size);
VOID mem_zero(VOID *target, SIZE_T size);
VOID mem_copy(VOID *dst, const VOID *src, SIZE_T size);
RAWBUF_ptr create_buffer(UINT32 size, BOOL extern_init);
STATUS destroy_buffer(RAWBUF_ptr buf, BOOL extern_init);
RAWBUF_ptr create_from_file(const CHAR *filename);
STATUS write_to_file(RAWBUF_ptr buf, const CHAR *filename);
/* Inline - moved to inlines.h */
//LOCAL INLINE STATUS write_1byte(RAWBUF_ptr buf, const UINT32 val);
//LOCAL INLINE STATUS write_2bytes(RAWBUF_ptr buf, const UINT32 val);
//LOCAL INLINE STATUS write_nbytes(RAWBUF_ptr buf, const CHAR *val, UINT32 count);

#ifdef __cplusplus
}
#endif
#endif /* UTILITIES_H */

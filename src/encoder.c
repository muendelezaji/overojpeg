/**
 * @file encoder.c
 *
 * @date 22 Dec 2011
 * @author abe
 */

#include "utilities.h"
#include "inlines.h"
#include "jpeg.h"
#include "encoder.h"

STATUS init_encoder(JPEG_ENC_ptr cjpg) {
	create_hufftables(cjpg->htbls, cjpg->is_colour);
	RETURN_STATUS(SUCCESS);
}

STATUS terminate_encoder(JPEG_ENC_ptr cjpg) {
    destroy_hufftables(cjpg->htbls, cjpg->is_colour);
    RETURN_STATUS(SUCCESS);
}

STATUS create_hufftables(HUFF_TBL_ptr *htbls, BOOL is_colour) {
    // Create Luminance Huffman tables
    htbls[DC_Y] = (HUFF_TBL_ptr)mem_request(sizeof(HUFF_TBL_obj));
    htbls[AC_Y] = (HUFF_TBL_ptr)mem_request(sizeof(HUFF_TBL_obj));
    derive_hufftable(BITS_DC_Y, HUFFVAL_DC_Y, htbls[DC_Y], FALSE);
    derive_hufftable(BITS_AC_Y, HUFFVAL_AC_Y, htbls[AC_Y], FALSE);

    // Create chrominance tables
    if(is_colour) {
        htbls[DC_UV] = (HUFF_TBL_ptr)mem_request(sizeof(HUFF_TBL_obj));
        htbls[AC_UV] = (HUFF_TBL_ptr)mem_request(sizeof(HUFF_TBL_obj));
        derive_hufftable(BITS_DC_UV, HUFFVAL_DC_UV, htbls[DC_UV], FALSE);
        derive_hufftable(BITS_AC_UV, HUFFVAL_AC_UV, htbls[AC_UV], FALSE);
    }

    RETURN_STATUS(SUCCESS);
}

STATUS destroy_hufftables(HUFF_TBL_ptr *htbls, BOOL is_colour) {
    mem_release(htbls[DC_Y], sizeof(HUFF_TBL_obj));
    mem_release(htbls[AC_Y], sizeof(HUFF_TBL_obj));
    if(is_colour) {
        mem_release(htbls[DC_UV], sizeof(HUFF_TBL_obj));
        mem_release(htbls[AC_UV], sizeof(HUFF_TBL_obj));
    }
    RETURN_STATUS(SUCCESS);
}

STATUS derive_hufftable(const UINT8 *bits, const UINT8 *huffval, HUFF_TBL_ptr htbl, const BOOL is_ac) {
    // DC - 12 useful - cat 0-11, AC - 162 useful - nz/cat {0-F}/{1-A}, 0/0, F/0
    UINT8 huffsize[NCODES_MAX + 1] = {0};
    UINT32 huffcode[NCODES_MAX + 1] = {0};
    UINT32 code, i, j, k, lastk, si;

    // Make table for code sizes (lengths)
    k = 0;
    for(i = 1; i <= 16; ++i) {
        for(j = bits[i - 1]; j > 0; --j) {
            huffsize[k++] = i;
        }
    }
    huffsize[k] = 0;
    lastk = k; // lastk is index of last entry +1 (=number of elements)

    // Table for actual codes
    code = k = 0;
    si = huffsize[0];
    while(huffsize[k]) {
        while(huffsize[k] == si) {
            huffcode[k++] = code++;
        }
        code <<= 1;
        si++;
    }

    // Arrange elements in increasing code length
    mem_zero(htbl->ehufsi, sizeof(UINT8) * NCODES_MAX);
    mem_zero(htbl->ehufco, sizeof(UINT32) * NCODES_MAX);
    for(k = 0; k < lastk; ++k) {
        i = huffval[k];
        htbl->ehufco[i] = huffcode[k];
        htbl->ehufsi[i] = huffsize[k];
    }

    RETURN_STATUS(SUCCESS);
}

STATUS mcu_block_encode(JPEG_ENC_ptr cjpg, const HUFF_TBL_ptr dctbl, const HUFF_TBL_ptr actbl, const INT32 *block, INT32 dc_prev) {
    register UINT32 i, j, rlz, nbits;
    register INT32 curr, bit_seq;

    // DC value encoding
    bit_seq = curr = *block - dc_prev;
    if(curr < 0) {
        curr = -curr;
        bit_seq--; // -ve values are bitwise ~ of input. Assumes 2's complement
    }
    nbits = 0;
    while(curr) {
        nbits++;
        curr >>= 1;
    }

    // Write Huffman code of the bits
    add_to_buffer(cjpg->dst, dctbl->ehufco[nbits], dctbl->ehufsi[nbits]);
    if(nbits) {
        add_to_buffer(cjpg->dst, (UINT32)bit_seq, nbits);
    }

    // AC values encoding
    rlz = 0; // Run length of consecutive zeros
    for(i = 1; i < MCU_SQUARE; ++i) {
        if((curr = block[SCANORDER[i]]) == 0) { // Read DCT coeffs in zig-zag order
            rlz++;
        }
        else {
            // Output special code for zero runlength (0xF0)
            while(rlz >= 16) {
                add_to_buffer(cjpg->dst, actbl->ehufco[0xF0], actbl->ehufsi[0xF0]);
                rlz -= 16;
            }

            bit_seq = curr;
            if(curr < 0) {
                curr = -curr;
                bit_seq--; // -ve values are bitwise ~ of input. Assumes 2's complement
            }
            nbits = 1; // Must have at least one bit
            while(curr >>= 1) {
                nbits++;
            }
            j = ((rlz << 4) | nbits) & 0xFF; // nibbles - upper: runlength, lower: category
            add_to_buffer(cjpg->dst, actbl->ehufco[j], actbl->ehufsi[j]);
            add_to_buffer(cjpg->dst, (UINT32)bit_seq, nbits);
            rlz = 0;
        }
    }
    // Output EOB code
    if(rlz) {
        add_to_buffer(cjpg->dst, actbl->ehufco[0], actbl->ehufsi[0]);
    }
    RETURN_STATUS(SUCCESS);
}

STATUS flush_buffer(RAWBUF_ptr stream) {
    // Fill incomplete byte with 1's at the end of Huffman scan
    add_to_buffer(stream, 0x7F, 7);
    stream->bit_accum = 0;
    stream->bits_left = 0;
    RETURN_STATUS(SUCCESS);
}

LOCAL INLINE STATUS add_to_buffer(RAWBUF_ptr stream, const UINT32 code, const UINT32 size) {
    register UINT32 c, outbuf = code;
    register UINT32 outbits = stream->bits_left; // No. of bits left to write out
    if(!size) {
        error_exit("add_to_buffer()", FAILURE, MSG_INVALIDHUFF);
    }

    outbuf &= (1L << size) - 1; // Mask extra bits
    outbits += size;
    outbuf <<= 24 - outbits;
    outbuf |= stream->bit_accum; // Accumulated bits to be written out

    while(outbits >= 8) {
        c = ((outbuf >> 16) & 0xFF);
        write_1byte(stream, c);
        if(c == 0xFF) {
            write_1byte(stream, 0); // Pad 0xFF with 0-byte to indicate non-marker
        }
        outbuf <<= 8;
        outbits -= 8;
    }

    stream->bit_accum = outbuf;
    stream->bits_left = outbits;
    RETURN_STATUS(SUCCESS);
}

/**
 * @file utilities.c
 * @brief Provides utility functions like memory de/allocation and error handling
 *
 * @date 24 Dec 2011
 * @author abe
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "utilities.h"

#ifdef DEBUG
/**
 * Simple check if all calls to mem_request have a corresponding one to mem_release.
 * Does not check byte count, that needs mem_release to know size being free
 * Update 25/03/2012:
 * Functions mem_{request,release} updated, mem_count now checks byte count
 * allocated and released. Make sure to pass in the same size to mem_release as
 * that used in mem_request.
 */
GLOBAL INT32 mem_count = 0;
#endif

VOID error_exit(const CHAR *function, INT32 code, const CHAR *message) {
    fprintf(stderr, "ERROR in %s: %s\n", function, message);
    exit(code);
}

VOID *mem_request(SIZE_T size) {
    VOID *ptr = malloc(size);
#ifdef DEBUG
    fprintf(stderr, "%d\t+\t%d\t=\t%d\n", mem_count, size, mem_count + size);
    mem_count += size;
#endif
    if(ptr == NULL)
        error_exit("mem_request()", FAILURE, MSG_MEMALLOC);
    return ptr;
}

VOID mem_release(VOID *ptr, SIZE_T size) {
    if ((ptr) != NULL) {
#ifdef DEBUG
        fprintf(stderr, "%d\t-\t%d\t=\t%d\n", mem_count, size, mem_count - size);
        mem_count -= size;
#endif
        free(ptr);
        ptr = NULL;
    }
}

VOID mem_zero(VOID *target, SIZE_T size) {
    memset((VOID *)target, 0, size);
}

VOID mem_copy(VOID *dst, const VOID *src, SIZE_T size) {
    memcpy((VOID *)dst, (const VOID *)src, size);
}

/**
 * Creates an empty buffer data of requested size. If is_extern is TRUE(1),
 * only the buffer pointer is returned, no space is created for the data itself.
 * Callers should ensure buf.data is assigned to some data before using it.
 */
RAWBUF_ptr create_buffer(UINT32 size, BOOL is_extern) {
    RAWBUF_ptr buf = (RAWBUF_ptr)mem_request(sizeof(RAWBUF_obj));
    if(!is_extern) {
        // create space for data
        buf->data = (UINT8 *)mem_request(size * sizeof(UINT8)); // allocate space
        buf->free = size;
    }
    else {
        // data pointer should be initialised by caller
        buf->data = NULL;
        buf->free = 0;
    }
    buf->size = size;
    buf->pos = 0;
    buf->bit_accum = buf->bits_left = 0; // Specific for output buffer
    return buf;
}

/**
 * Frees memory used by a buffer. If is_extern is TRUE(1) buf->data is not
 * freed and that should be done by caller.
 */
STATUS destroy_buffer(RAWBUF_ptr buf, BOOL is_extern) {
    if(!is_extern) {
        mem_release(buf->data, buf->size);
    }
    else {
        buf->data = NULL;
    }
    mem_release(buf, sizeof(RAWBUF_obj));
    RETURN_STATUS(SUCCESS);
}

RAWBUF_ptr create_from_file(const CHAR *filename) {
	UINT32 size, position;
    RAWBUF_ptr buf = (RAWBUF_ptr)mem_request(sizeof(RAWBUF_obj));
    // Open file for reading
    FILE *fptr = fopen(filename, "rb");
    if(fptr == NULL) {
        error_exit("create_from_file()", FAILURE, MSG_FILEOPEN);
    }

    // Get file size
    position = 0;
    fseek(fptr, 0, SEEK_END);
    size = ftell(fptr);
    rewind(fptr); // Return to start of file - equivalent to fseek(fptr, 0, SEEK_SET)

    // Allocate memory to read in data
    buf->data = (UINT8 *)mem_request(size * sizeof(UINT8)); // allocate space
    position = fread(buf->data, 1, size, fptr);
    if(position != size)
        error_exit("create_from_file()", FAILURE, MSG_FILEREAD);

    // Reset position pointer and close file
    buf->free = buf->pos = 0;
    buf->size = size;
    fclose(fptr);
    return buf;
}

STATUS write_to_file(RAWBUF_ptr buf, const CHAR *filename) {
	FILE *fptr = fopen(filename, "wb+");
	if(fptr == NULL) {
		error_exit("write_to_file()", FAILURE, MSG_FILEOPEN);
	}
	fwrite(buf->data, 1, buf->size - buf->free, fptr);
	fclose(fptr);
	RETURN_STATUS(SUCCESS);
}

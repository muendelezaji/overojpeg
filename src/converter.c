/**
 * @file converter.c
 * @brief Colour conversion between RGB and YUV colourspaces. Note that the
 * particular YUV variant used here is the YCbCr with 4:4:4 without subsampling.
 *
 * @date 22 Dec 2011
 * @author abe
 */

#include "utilities.h"
#include "jpeg.h"
#include "converter.h"

STATUS init_converter(JPEG_ENC_ptr cjpg) {
    create_rgb2yuv_table(&cjpg->cctbl);
    cjpg->is_colour = TRUE;
    cjpg->precision = 8;
    switch(cjpg->cs) {
        // Grayscale images
        case CS_GRAY:
            cjpg->is_colour = FALSE;
            break;
        // Convert RGB to YCbCr
        case CS_RGB:
            break;
        // YCbCr images can be used directly
        case CS_YUV:
            break;
        // Unidentified colour space
        case CS_UNKNOWN:
        default:
            break;
    }
    RETURN_STATUS(SUCCESS);
}

STATUS terminate_converter(JPEG_ENC_ptr cjpg) {
    destroy_rgb2yuv_table(cjpg->cctbl);
    RETURN_STATUS(SUCCESS);
}

/**
 * Pad input width and height to multiples of MCU size (8, in this case)
 * Allocate space and arrange image in the layout expected by rest of encoder
 */
STATUS jpeg_preprocess(JPEG_ENC_ptr cjpg) {
    // alternative way to get next multiple of 8 higher than N: (N + 7) & ~7
    register INT32 padw = cjpg->width + cjpg->width % MCU_SIZE;
    register INT32 padh = cjpg->height + cjpg->height % MCU_SIZE;
    register INT32 i, pos, col, row, skip;
    INT32 scan_size = padw * padh * (INT32)cjpg->precision / 8;
    JPEG_SAMPLE lastpix;
    JPEG_SAMPLE_ptr scan[3], lastrow;
    JPEG_SAMPLE_ptr in_data = cjpg->src->data;
    COLOUR_TBL_ptr cctbl = cjpg->cctbl;

    // Some grayscale images have their 3 components repeated - need only 1 scan
    for(i = 0; i < cjpg->components; ++i) {
        scan[i] = (JPEG_SAMPLE_ptr)mem_request(scan_size);
        //mem_zero(scan[i], scan_size);
        if(cjpg->cs == CS_GRAY)
            break;
    }

    switch(cjpg->cs) {
        register INT32  r, g, b, y, u, v;
        case CS_GRAY:
            skip = cjpg->components - 1;
            for(row = 0; row < cjpg->height; ++row) {
                pos = row * (INT32)cjpg->width;
                for(col = 0; col < cjpg->width; ++col) {
                    scan[i][pos + col] = in_data[pos + col + skip];
                }
            }
            cjpg->components = 1; // Rest of application expects 1-component grayscale
            break;
        case CS_RGB:
            for(row = 0; row < cjpg->height; ++row) {
                pos = row * (INT32)cjpg->width;
                for(col = 0; col < cjpg->width; ++col) {
                    r = in_data[RGB_R];
                    g = in_data[RGB_G];
                    b = in_data[RGB_B];
                    in_data += 3; // number of input components
                    y = cctbl[r + Y_R] + cctbl[g + Y_G] + cctbl[b + Y_B];
                    u = cctbl[r + CB_R] + cctbl[g + CB_G] + cctbl[b + CB_B];
                    v = cctbl[r + CR_R] + cctbl[g + CR_G] + cctbl[b + CR_B];
                    scan[YUV_Y][pos + col] = UNFIXED(y); //CLAMP(y, 0, 255);
                    scan[YUV_U][pos + col] = UNFIXED(u); //CLAMP(u, 0, 255);
                    scan[YUV_V][pos + col] = UNFIXED(v); //CLAMP(v, 0, 255);
                }
            }
            break;
        // No conversion needed
        case CS_YUV:
            for(row = 0; row < cjpg->height; ++row) {
                pos = row * (INT32)cjpg->width;
                for(col = 0; col < cjpg->width; ++col) {
                    scan[YUV_Y][pos + col] = in_data[YUV_Y];
                    scan[YUV_U][pos + col] = in_data[YUV_U];
                    scan[YUV_V][pos + col] = in_data[YUV_V];
                }
            }
            break;
        // Unknown/unhandled colour space
        default:
            break;
    }

    // Apply padding to make image integral multiple of 8x8 blocks/pixels
    for(i = 0; i < cjpg->components; ++i) {
        // Horizontal padding by copying last pixel on the row
        pos = cjpg->width * cjpg->height;
        lastpix = scan[i][pos - 1];
        for(col = cjpg->width; col < padw; ++col) {
            scan[i][pos + col] = lastpix;
        }
        // Vertical padding by copying bottom row
        lastrow = &scan[i][pos - padw];
        for(row = cjpg->height; row < padh; ++row) {
            pos = row * (INT32)padw;
            mem_copy(&scan[i][pos], lastrow, padw);
        }
    }

    // Assign to corresponding JPEG_ENC object scan
    for(i = 0; i < cjpg->components; ++i) {
        if(scan[i] != NULL) {
            cjpg->scans[i] = scan[i];
        }
    }

    // Downsample if needed
    if(cjpg->sf != SF_NONE) {
        downsample(cjpg->sf);
    }
    RETURN_STATUS(SUCCESS);
}

STATUS create_rgb2yuv_table(COLOUR_TBL_ptr *cctbl) {
    COLOUR_TBL_ptr table = (COLOUR_TBL_ptr)mem_request(CCTBL_SIZE * sizeof(COLOUR_TBL_obj));
    INT32 i;
    // Table includes Cb/Cr centre offset(+128) and float rounding off factor(+0.5)
    for(i = 0; i < MAX_COLOR; ++i) {
        table[i + Y_R] = FIXEDPT(0.29900) * i;
        table[i + Y_G] = FIXEDPT(0.58700) * i;
        table[i + Y_B] = FIXEDPT(0.11400) * i + FIXHALF;
        table[i + CB_R] = -(FIXEDPT(0.16874)) * i;
        table[i + CB_G] = -(FIXEDPT(0.33126)) * i;
        table[i + CB_B] = FIXEDPT(0.50000) * i + FIX128 + FIXHALF - 1;
        // table[i + OFF_CR_R] = table[i + OFF_CB_B]; // Same as OFF_CB_B
        table[i + CR_G] = -(FIXEDPT(0.41869)) * i;
        table[i + CR_B] = -(FIXEDPT(0.08131)) * i;
    }
    *cctbl = table;
    RETURN_STATUS(SUCCESS);
}

STATUS destroy_rgb2yuv_table(COLOUR_TBL_ptr cctbl) {
    mem_release(cctbl, CCTBL_SIZE * sizeof(COLOUR_TBL_obj));
    RETURN_STATUS(SUCCESS);
}

STATUS downsample(INT32 format) {
    RETURN_STATUS(SUCCESS);
}

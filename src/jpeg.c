/**
 * @file jpeg.c
 *
 * @date 22 Dec 2011
 * @author abe
 */

#ifdef DEBUG
#include <stdio.h>
#endif
#include "utilities.h"
#include "inlines.h"
#include "converter.h"
#include "quantiser.h"
#include "encoder.h"
#include "jpeg.h"

/**
 * Main entry point for the encoder. Image parameters are passed via the a pointer
 * to a JPEG_PARAMS_obj struct. Initialisation of the encoder may be done by the
 * caller (external) or left to the compression process. External initialisation
 * allows one encoder to run multiple compression processes (without reinitiliation)
 * @param[in] data          Pointer to uncompressed byte data (expects UINT8)
 * @param[in] params        Image parameters
 * @param[in,out] cjpg      Encoder instance pointer
 * @param[in] extern_init   Encoder is already initalised by caller (TRUE), or not (FALSE)
 * @param[in] filename      Name of file to save JPEG image in
 */
STATUS jpeg_compress(UINT8 *indata, JPEG_PARAMS_obj *params, JPEG_ENC_obj *cjpg, CHAR *filename, BOOL extern_init) {
    // Create encoder object with some parameters
    cjpg->width = params->width;
    cjpg->height = params->height;
    cjpg->quality = params->quality;

    // At the moment params.colourspace is ignored
    // Also, if no. of components < 3, grascale is assumed
    if(params->components < 3) {
        cjpg->components = 1;
        cjpg->cs = CS_GRAY;
    }
    else {
        cjpg->components = 3;
        cjpg->cs = (params->colourspace == CS_UNKNOWN)? CS_RGB : params->colourspace;
    }
#ifdef DEBUG
    fprintf(stdout, "In jpeg_compress(): Starting the encoder.\n");
#endif

    if(!extern_init) {
        // Initialise the compression process
        jpeg_init_compressor(cjpg);
    }

    // Initialise source and output buffers
    // estimate for input = width * height * no. of components
    // estimate for output(very generous) = quality * size of uncompressed data
    UINT32 est_size = params->width * params->height * params->components;
    cjpg->src = create_buffer(est_size, params->extern_buffer);
    cjpg->dst = create_buffer(est_size * params->quality / 100L, 0);
    cjpg->src->data = indata;

    // colour conversion, 8x8 expansion, layout in scans
    jpeg_preprocess(cjpg);

    // Create compressed scan data
    jpeg_write_scans(cjpg);

    // Write compressed image to file
    write_to_file(cjpg->dst, filename);

    INT32 i;
    INT32 padw = cjpg->width + cjpg->width % MCU_SIZE;
    INT32 padh = cjpg->height + cjpg->height % MCU_SIZE;
    INT32 scan_size = padw * padh * (INT32)cjpg->precision / 8;
    for(i = 0; i < cjpg->components; ++i) {
        mem_release(cjpg->scans[i], scan_size);
    }

    // Release buffers
    destroy_buffer(cjpg->src, params->extern_buffer);
    destroy_buffer(cjpg->dst, 0);

    if(!extern_init) {
        // Finish and clean up
        jpeg_terminate_compressor(cjpg);
    }

#ifdef DEBUG
    fprintf(stdout, "In jpeg_compress(): Finished encoding.\n");
#endif

    RETURN_STATUS(SUCCESS);
}

/* Initialise modules */
STATUS jpeg_init_compressor(JPEG_ENC_ptr cjpg) {
    init_converter(cjpg);
    init_quantiser(cjpg);
    init_encoder(cjpg);
    RETURN_STATUS(SUCCESS);
}

/* Terminate modules */
STATUS jpeg_terminate_compressor(JPEG_ENC_ptr cjpg) {
    terminate_converter(cjpg);
    terminate_quantiser(cjpg);
    terminate_encoder(cjpg);
    RETURN_STATUS(SUCCESS);
}

STATUS jpeg_write_scans(JPEG_ENC_ptr cjpg) {
    // Create JPEG metadata
    write_headers(cjpg);

    /* Run this per MCU block */
    UINT32 pos, y, x, y2d, off, mcu_row, mcu_col, r, c, id, comp;
    UINT32 mcu_x = cjpg->width / 8;
    UINT32 mcu_y = cjpg->height / 8;
    INT32 in_block[64], dct_out[64], quant_out[64];
    INT32 prev_dc[3] = {0};
    for(r = 0; r < mcu_y; ++r) {
        mcu_row = r * 8 * cjpg->width;
        for(c = 0; c < mcu_x; ++c) {
            mcu_col = c * 8;
            off = mcu_row + mcu_col;
            for(comp = 0; comp < cjpg->components; ++comp) {
                id = (comp == 0)? 0 : 2;
                for(y = 0; y < 8; ++y) {
                    y2d = y * cjpg->width;
                    pos = y * 8;
                    for(x = 0; x < 8; ++x) {
                        in_block[pos + x] = (INT32)cjpg->scans[comp][off + y2d + x];
                    }
                } // finished building MCU in_block, now work on it
                mcu_block_fdct(cjpg, in_block, dct_out); // FDCT
                mcu_block_quantise(cjpg, cjpg->qtbls[id >> 1], dct_out, quant_out); // Quantise
                mcu_block_encode(cjpg, cjpg->htbls[id], cjpg->htbls[id + 1], quant_out, prev_dc[comp]); // Lossless encode
                prev_dc[comp] = quant_out[0]; // assign previous DC value
            } // finished 1 component, next up
        }
    }
    flush_buffer(cjpg->dst); // fill up incomplete byte with 1's

    /* Finished scans */
    // Define Number of Lines - end of first scan only (optional?)
    //write_marker(cjpg, JPEG_DNL);

    // Write End Of Image marker
    write_marker(cjpg, MK_EOI);
    RETURN_STATUS(SUCCESS);
}

LOCAL STATUS write_headers(JPEG_ENC_ptr cjpg) {
    write_marker(cjpg, MK_SOI); // start of image
    write_jfif_header(cjpg); // APP0 section
    write_comment(cjpg, "overojpeg by abe 2012", 21); // comment section, no. of chars
    write_quant_tables(cjpg); // quantisation tables
    write_huff_tables(cjpg); // Huffman tables
    write_frame_header(cjpg);
    write_scan_header(cjpg);
    RETURN_STATUS(SUCCESS);
}

LOCAL STATUS write_quant_tables(JPEG_ENC_ptr cjpg) {
    INT32 n, i, val;
    INT32 count = (cjpg->is_colour)? 2 : 1;
    INT32 length = 67; // 2 + 65 * count;
    UINT8 table[MCU_SQUARE];
    for(n = 0; n < count; ++n) {
        write_marker(cjpg, MK_DQT); // quantisation tables marker
        write_2bytes(cjpg->dst, length); // segment length
        write_1byte(cjpg->dst, (cjpg->precision & 0x10) | n); // table ID nibbles: high - precision(8/16-bit), low - luma/chroma
        for(i = 0; i < MCU_SQUARE; ++i) {
            val = (INT32)cjpg->qtbls[n]->qcoeff[SCANORDER[i]];
            //val = (1 - val) * (INT32)cjpg->quality / 50 + 2 * val - 1;
            table[i] = CLAMP(val, 1, 255);
            //write_1byte(cjpg->dst, CLAMP(val, 1, 255));
        }
        write_nbytes(cjpg->dst, (CHAR *)table, 64); // write all at once
    }
    RETURN_STATUS(SUCCESS);
}

LOCAL STATUS write_huff_tables(JPEG_ENC_ptr cjpg) {
    HSPEC_TBL_obj hspec[4];
    INT32 i, id, length = 0, count = 2;
    mem_copy(hspec[0].bits, BITS_DC_Y, 16);
    mem_copy(hspec[0].huffval, HUFFVAL_DC_Y, 12);
    mem_copy(hspec[1].bits, BITS_AC_Y, 16);
    mem_copy(hspec[1].huffval, HUFFVAL_AC_Y, 162);
    if(cjpg->is_colour) {
        count = 4;
        mem_copy(hspec[2].bits, BITS_DC_UV, 16);
        mem_copy(hspec[2].huffval, HUFFVAL_DC_UV, 12);
        mem_copy(hspec[3].bits, BITS_AC_UV, 16);
        mem_copy(hspec[3].huffval, HUFFVAL_AC_UV, 162);
    }
    for(i = 0; i < count; ++i) {
        id = ((i & 0x01) << 4) | ((i & 0x02) >> 1); // table ID nibbles: high - DC/AC, low - Y/UV
        length = (i & 0x01)? 181 : 31; // = size(2) + id(1) + nbits(16) + 12(dc)/162(ac)
        write_marker(cjpg, MK_DHT); // Huffman tables marker
        write_2bytes(cjpg->dst, length);
        write_1byte(cjpg->dst, id);
        write_nbytes(cjpg->dst, (CHAR *)hspec[i].bits, 16);
        write_nbytes(cjpg->dst, (CHAR *)hspec[i].huffval, length - 19);
    }

    /* Alternative way of writing the tables */
    /*
    //INT32 j, k, l;
    write_marker(cjpg, MK_DHT); // Huffman tables marker
    for(i = 0; i < count; ++i) {
        for(k = 0; k < 16; ++k) {
            length += hspec[i].bits[k];
        }
    }
    length += 36 * (count / 2); // 2*nbits(16) + 2*id(2) for each of Y and UV
    write_2bytes(cjpg->dst, length);
    for(i = 0; i < count; ++i) {
        id = ((i & 0x01) << 4) | ((i & 0x02) >> 1); // table ID: high nibble - DC/AC, low nibble - Y/UV
        write_1byte(cjpg->dst, id);
        for(k = 0; k < 16; ++k) {
            write_1byte(cjpg->dst, hspec[i].bits[k]);
        }
        for(k = 0, l = 0; k < 16; ++k) {
            for(j = 0; j < hspec[i].bits[k]; ++j) {
                write_1byte(cjpg->dst, hspec[i].huffval[l++]);
            }
        }
    }
    */
    RETURN_STATUS(SUCCESS);
}

LOCAL INLINE STATUS write_marker(JPEG_ENC_ptr cjpg, JPEG_MARKER marker) {
    write_2bytes(cjpg->dst, (0xFF00 | marker));
    RETURN_STATUS(SUCCESS);
}

LOCAL INLINE STATUS write_jfif_header(JPEG_ENC_ptr cjpg) {
    write_marker(cjpg, MK_APP0); // APP0 segment marker
    write_2bytes(cjpg->dst, 16); // length of segment (in bytes)
    write_nbytes(cjpg->dst, "JFIF", 4);
    write_1byte(cjpg->dst, 0); // null termination for "JFIF"
    write_2bytes(cjpg->dst, 0x0101); // version (major, minor)
    write_1byte(cjpg->dst, 0); // density units (0 = aspect ratio)
    write_2bytes(cjpg->dst, 1); // horizontal a:r
    write_2bytes(cjpg->dst, 1); // vertical a:r
    write_2bytes(cjpg->dst, 0); // thumbnail width & height (none)
    RETURN_STATUS(SUCCESS);
}

LOCAL INLINE STATUS write_comment(JPEG_ENC_ptr cjpg, const CHAR *comment, const UINT32 length) {
    write_marker(cjpg, MK_COM); // comment segment marker
    write_2bytes(cjpg->dst, length + 1); // +1 for null termination
    write_nbytes(cjpg->dst, comment, length);
    write_1byte(cjpg->dst, 0);
    RETURN_STATUS(SUCCESS);
}

LOCAL INLINE STATUS write_frame_header(JPEG_ENC_ptr cjpg) {
    INT32 i;
    INT32 length = 8 + 3 * (INT32)cjpg->components;
    write_marker(cjpg, MK_SOF0); // start of frame
    write_2bytes(cjpg->dst, length); // length of segment
    write_1byte(cjpg->dst, cjpg->precision);
    write_2bytes(cjpg->dst, cjpg->height);
    write_2bytes(cjpg->dst, cjpg->width);
    write_1byte(cjpg->dst, cjpg->components);
    for(i = 0; i < cjpg->components; ++i) {
        write_1byte(cjpg->dst, i + 1); // component ID
        write_1byte(cjpg->dst, 0x11); // sampling factors (hsamp << 4 | vsamp) = 0x22?
        write_1byte(cjpg->dst, (i == 0)? 0 : 1); // quantisation table ID
    }
    RETURN_STATUS(SUCCESS);
}

LOCAL INLINE STATUS write_scan_header(JPEG_ENC_ptr cjpg) {
    INT32 i;
    INT32 length = 6 + 2 * (INT32)cjpg->components;
    write_marker(cjpg, MK_SOS); // start of scan
    write_2bytes(cjpg->dst, length); // segment length
    write_1byte(cjpg->dst, cjpg->components);
    for(i = 0; i < cjpg->components; ++i) {
        write_1byte(cjpg->dst, i + 1); // component ID
        write_1byte(cjpg->dst, (i == 0)? 0 : 0x11); // hufftable IDs (dc << 4 | ac)
    }
    write_1byte(cjpg->dst, 0); // ss
    write_1byte(cjpg->dst, 63); // se
    write_1byte(cjpg->dst, 0); // (ah << 4 | al)
    RETURN_STATUS(SUCCESS);
}

/**
 * @file quantiser.c
 *
 * @date 22 Dec 2011
 * @author abe
 */

#include <math.h>
#include "utilities.h"
#include "jpeg.h"
#include "quantiser.h"

STATUS init_quantiser(JPEG_ENC_ptr cjpg) {
	create_quant_tables(cjpg->qtbls, QTABLE_Y, QTABLE_UV, cjpg->quality, cjpg->is_colour);
	RETURN_STATUS(SUCCESS);
}

STATUS terminate_quantiser(JPEG_ENC_ptr cjpg) {
	destroy_quant_tables(cjpg->qtbls, cjpg->is_colour);
	RETURN_STATUS(SUCCESS);
}

STATUS create_quant_tables(QUANT_TBL_ptr *qtbls, const UINT8 *luma, const UINT8 *chroma, const UINT32 quality, BOOL is_colour) {
    INT32 i, tmp;
    QUANT_TBL_ptr table;

    // Scale quality factor - uses non-linear curve recommended by the Independent JPEG group
    INT32 scale = CLAMP(quality, 1, 100L);
    scale = (scale > 50)? 200 - scale * 2 : 5000 / scale;

    // Create luminance quantisation table
    table = (QUANT_TBL_ptr)mem_request(sizeof(QUANT_TBL_obj));
    for(i = 0; i < MCU_SQUARE; ++i) {
        tmp = ((INT32)luma[i] * scale + 50L) / 100L;
        table->qcoeff[i] = CLAMP(tmp, 1, 255);
    }
    qtbls[LUMA] = table;

    // Create chrominance quantisation table
    if(is_colour) {
        table = (QUANT_TBL_ptr)mem_request(sizeof(QUANT_TBL_obj));
        for(i = 0; i < MCU_SQUARE; ++i) {
            tmp = ((INT32)chroma[i] * scale + 50L) / 100L;
            table->qcoeff[i] = CLAMP(tmp, 1, 255);
        }
        qtbls[CHROMA] = table;
    }
    RETURN_STATUS(SUCCESS);
}

STATUS destroy_quant_tables(QUANT_TBL_ptr *qtbls, BOOL is_colour) {
    mem_release(qtbls[LUMA], sizeof(QUANT_TBL_obj));
    if(is_colour) {
        mem_release(qtbls[CHROMA], sizeof(QUANT_TBL_obj));
    }
    RETURN_STATUS(SUCCESS);
}

STATUS mcu_block_fdct(JPEG_ENC_ptr cjpg, const INT32 *in, INT32 *out) {
    FLOAT coeffmat[MCU_SQUARE] = {0};
    register FLOAT au, av, *coeff = &coeffmat[0];
    register UINT32 x, y, u, v, i;
    for(v = 0; v < MCU_SIZE; ++v) {
        av = (v != 0)? 0.5F : M_SQRT1_8;
        for(u = 0; u < MCU_SIZE; ++u) {
            au = (u != 0)? 0.5F : M_SQRT1_8;
            //i = v * MCU_SIZE + u;
            for(y = 0; y < MCU_SIZE; ++y) {
                for(x = 0; x < MCU_SIZE; ++x) {
                    //coeffmat[i] += (*(in++) - 128L) * au * av * cos(M_PI1_16 * ((x << 1) + 1) * u) * cos(M_PI1_16 * ((y << 1) + 1) * v);
                    *coeff += (*(in++) - 128L) * au * av * cos(M_PI1_16 * ((x << 1) + 1) * u) * cos(M_PI1_16 * ((y << 1) + 1) * v);
                }
            }
            in -= MCU_SQUARE;
            coeff++;
        }
    }
    //coeff -= MCU_SQUARE;
    for(i = 0; i < MCU_SQUARE; ++i) {
        out[i] = ROUND(coeffmat[i]);
    }
    RETURN_STATUS(SUCCESS);
}

STATUS mcu_block_quantise(JPEG_ENC_ptr cjpg, const QUANT_TBL_ptr qtbl, const INT32 *in, INT32 *out) {
    register UINT32 i;
    for(i = 0; i < MCU_SQUARE; i++) {
        //out[i] = (INT32)(0.5 + in[i] * 100L / qtbl->qcoeff[i]);
        out[i] = (INT32)(in[i] + (qtbl->qcoeff[i] >> 1)) / qtbl->qcoeff[i];
    }
    RETURN_STATUS(SUCCESS);
}

/**
 * DCT test on an 8x8 block - uses the slowest, most accurate floating point method
 * as described in the standard. Faster alternatives include the AAN DCT as used in
 * faster libraries
 * author: abe 2011
 * -----------------------------------------------------------------------------
 * Expected output:
 * data1:
 * [ 1016    0    0    0    0    0    0    0 ]
 * [    0    0    0    0    0    0    0    0 ]
 * [    0    0    0    0    0    0    0    0 ]
 * [    0    0    0    0    0    0    0    0 ]
 * [    0    0    0    0    0    0    0    0 ]
 * [    0    0    0    0    0    0    0    0 ]
 * [    0    0    0    0    0    0    0    0 ]
 * [    0    0    0    0    0    0    0    0 ]
 * data2:
 * [ -415  -30  -61   27   56  -20   -2    0 ]
 * [    4  -22  -61   10   13   -7   -9    5 ]
 * [  -47    7   77  -25  -29   10    5   -6 ]
 * [  -49   12   34  -15  -10    6    2    2 ]
 * [   12   -7  -13   -4   -2    2   -3    3 ]
 * [   -8    3    2   -6   -2    1    4    2 ]
 * [   -1    0    0   -2   -1   -3    4   -1 ]
 * [    0    0   -1   -4   -1    0    1    2 ]
 * data3:
 * [ -218  -33   16   17   -1   22   -7    3 ]
 * [   15    6   -3    3    8   -1    4   11 ]
 * [   22   -8  -13   -6    6   13    0    3 ]
 * [   23   25  -20  -11   -5   -3    7   10 ]
 * [   12    5    5    5    7   21   30  -13 ]
 * [   15   -9  -27   27   -9    8   -5   15 ]
 * [   10    7   -5  -29   17  -19  -24   11 ]
 * [   13    6   -6   -2   21    1   15  -17 ]
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

typedef signed char        INT8;
typedef unsigned char      UINT8;
typedef short              INT16;
typedef unsigned short     UINT16;
typedef int                INT32;
typedef unsigned int       UINT32;
typedef long long          INT64;
typedef unsigned long long UINT64;
typedef float              FLOAT;

#define SAFE_FREE(x) do { if(x) free(x); x = 0; } while(0)

#define M_PI1_8      (0.39269908169872415481F)
#define M_PI1_16     (0.19634954084936207740F)
#define M_SQRT1_8    (0.35355339059327376220F)
#define MCU_SIZE     (8)
#define MCU_SQUARE   (64)
#define MCU_COLS     (MCU_SIZE)
#define MCU_ROWS     (MCU_SIZE)

/* All white pixels */
static const UINT8 data1[] = {
 255, 255, 255, 255, 255, 255, 255, 255,
 255, 255, 255, 255, 255, 255, 255, 255,
 255, 255, 255, 255, 255, 255, 255, 255,
 255, 255, 255, 255, 255, 255, 255, 255,
 255, 255, 255, 255, 255, 255, 255, 255,
 255, 255, 255, 255, 255, 255, 255, 255,
 255, 255, 255, 255, 255, 255, 255, 255,
 255, 255, 255, 255, 255, 255, 255, 255,
};

/* Sample data courtesy of Wikipedia */
static const UINT8 data2[] = {
 52, 55, 61,  66,  70,  61, 64, 73,
 63, 59, 55,  90, 109,  85, 69, 72,
 62, 59, 68, 113, 144, 104, 66, 73,
 63, 58, 71, 122, 154, 106, 70, 69,
 67, 61, 68, 104, 126,  88, 68, 70,
 79, 65, 60,  70,  77,  68, 58, 75,
 85, 71, 64,  59,  55,  61, 65, 83,
 87, 79, 69,  68,  65,  76, 78, 94
};

/*
 Sample data courtesy of
 http://web.archive.org/web/20030605143940/http://web.usxchange.net/elmo/jpeg.htm
 */
static const UINT8 data3[] = {
 118,  91, 123, 125, 124, 114, 117, 104,
 100,  93,  88,  85,  87, 100, 115, 105,
  94, 101,  78,  99, 112,  87, 109, 115,
 104,  82,  83,  78,  92, 118, 124,  88,
 119,  93,  99, 104,  76, 111,  86, 112,
  83, 119, 104,  92, 111,  76, 112, 100,
  97,  85,  90, 100, 106, 125, 123,  86,
  99,  86,  85, 105,  81,  99, 115, 119,
};

INT32 *block_fdct(const UINT8 *in) {
#ifdef USEINT
    INT32 *out = (INT32 *)malloc(MCU_SQUARE * sizeof(INT32));
#else
    FLOAT *out = (FLOAT *)malloc(MCU_SQUARE * sizeof(FLOAT));
    INT32 *int_out = (INT32 *)malloc(MCU_SQUARE * sizeof(INT32));
#endif
    INT32 i, j, x, y, u, v;
    FLOAT au, av;
    
    // Print level shifted input (-128 of original)
    printf("Shifted input before FDCT:\n");
    for(v = 0; v < MCU_ROWS; ++v) {
        printf("[ ");
        for(u = 0; u < MCU_COLS; ++u) {
            printf("%3d, ", *(in + v * MCU_COLS + u) - 128);
        }
        printf("]\n");
    }
    printf("\n");
    
#ifndef FDCT2D
    // Do the 2D FDCT calculation
    for(v = 0; v < MCU_SIZE; ++v) {
        av = (v != 0)? 0.5F : M_SQRT1_8;
        for(u = 0; u < MCU_SIZE; ++u) {
            au = (u != 0)? 0.5F : M_SQRT1_8;
            j = v * MCU_SIZE + u;
            for(y= 0; y < MCU_SIZE; ++y) {
                for(x = 0; x < MCU_SIZE; ++x) {
                    i = y * MCU_SIZE + x;
                    // *(out + j) += (*(in + i) - 128) * au * av * cos(M_PI1_16 * ((x << 1) + 1) * u) * cos(M_PI1_16 * ((y << 1) + 1) * v);
                    *out += (*(in++) - 128) * au * av * cos(M_PI1_16 * ((x << 1) + 1) * u) * cos(M_PI1_16 * ((y << 1) + 1) * v);
                }
            }
            in -= MCU_SQUARE;
            out++;
        }
    }
#else
    // Alternatively, find the FDCT of each row independently
    // and then the FDCT for each column of the resulting block
    // Rows
    FLOAT *tmp = (FLOAT*)malloc(MCU_COLS * sizeof(FLOAT)); // Temporary store for each row/column
    for(i = 0; i < MCU_ROWS; ++i) { // For each row
        for(u = 0; u < MCU_COLS; ++u) { // For each element in that row
            au = (u != 0)? 0.5F : M_SQRT1_8;
            // DCT coefficient[u] = sum (row elements cosines * values)
            for(x = 0; x < MCU_COLS; ++x) {
                *tmp += (*(in++) - 128) * au * cosf(M_PI1_16 * ((x << 1) + 1) * u);
            } // 1 row element
            *(out++) = *(tmp++);
            in -= MCU_COLS; // Reset input to start of row for next element
        } // End of row
        // Reset temporary store and advance input for next round
        tmp -= MCU_COLS;
        in += MCU_COLS;
    } // End of block
    
    // Reset output pointer to start of block
    out -= MCU_ROWS * MCU_COLS;
    
    // Columns
    for(i = 0; i < MCU_COLS; ++i) { // For each column
        for(v = 0; v < MCU_ROWS; ++v) { // For each element in that column
            av = (v != 0)? 0.5F : M_SQRT1_8;
            // DCT coefficient[v] = sum (column elements cosines * values)
            for(y = 0; y < MCU_ROWS; ++y) {
                j = y * MCU_COLS + v;
                *tmp += *(out + j) * av * cosf(M_PI1_16 * ((y << 1) + 1) * v);
            } // 1 column element
            tmp++; // *(out + j) = *(tmp++);
        } // End of column
        tmp -= MCU_ROWS;
        for(y = 0; y < MCU_ROWS; ++y) {
            *(out + y * MCU_COLS + i) = *(tmp + y);
        }
    } // End of block
    SAFE_FREE(tmp);
#endif
    out -= MCU_SQUARE; // Reset to beginning
#ifdef USEINT
    return out;
#else
    // Round off values
    for(i = 0; i < MCU_SQUARE; ++i)
        int_out[i] = round(out[i]);
    return int_out;
#endif
}

int blockdcttest() {
    INT32 *result; // = (INT32*)malloc(MCU_SQUARE * sizeof(INT32));
    printf("Using integers\n");
    UINT32 x, y;
    
    // Print input data matrix
    printf("Input 8x8 data:\n");
    for(y = 0; y < MCU_ROWS; ++y) {
        printf("[ ");
        for(x = 0; x < MCU_COLS; ++x) {
            printf("%3d, ", *(data1 + y * MCU_COLS + x));
        }
        printf("]\n");
    }
    printf("\n");
    
    // Perform FDCT on matrix
    result = block_fdct(data1);
    
    // Print resulting matrix
    printf("Output data after FDCT:\n");
    for(y = 0; y < MCU_ROWS; ++y) {
        printf("[ ");
        for(x = 0; x < MCU_COLS; ++x) {
            printf("% 7d ", *(result + y * MCU_COLS + x));
            //printf("% 7.2f ", *(result + y * MCU_COLS + x));
        }
        printf("]\n");
    }
    printf("\n");
    
    SAFE_FREE(result);
    return 0;
}


/**
 * Represents IEEE754 single precision floats as equivalent 32-bit hexadecimal numbers
 * author: abe
 * Expected output:
 * -----------------------------------------------------------------------------
 * numel: 9
 * i:   float  *(int*)&f[i] *(int*)(f+i)    hex
 * 0:  0.29900   1050220167   1050220167 0x3E991687
 * 1:  0.58700   1058424226   1058424226 0x3F1645A2
 * 2:  0.11400   1038710997   1038710997 0x3DE978D5
 * 3: -0.16874  -1104360914  -1104360914 0xBE2CCA2E
 * 4:  0.33126   1051302633   1051302633 0x3EA99AE9
 * 5:  0.50000   1056964608   1056964608 0x3F000000
 * 6: -0.50000  -1090519040  -1090519040 0xBF000000
 * 7:  0.41868   1054235962   1054235962 0x3ED65D3A
 * 8: -0.08131  -1113160229  -1113160229 0xBDA685DB
*/

#include <stdio.h>

int float2hex() {
  float f[] = { 0.29900F, 0.58700F, 0.11400F, -0.16874F, 0.33126F, 0.50000F, -0.50000F, 0.41868F, -0.08131F };
  int i=0, numel=(int)(sizeof(f)/sizeof(float));
  printf("numel: %d\n", numel);
  printf("i: %7s  %12s %12s %6s\n", "float", "*(int*)&f[i]", "*(int*)(f+i)", "hex");
  for(; i<numel; ++i) {
    int f_arr = *(int*)&f[i], f_ptr = *(int*)(f+i);
    printf("%d: % 2.5f %12d %12d 0x%08X\n", i, i%2?f[i]:*(f+i), f_arr, f_ptr, i%2?f_arr:f_ptr);
  }
  return 0;
}


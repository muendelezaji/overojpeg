/**
 * Converts float numbers to fixed-point integers, operates on them and prints rounded values
 * Used to create the colour conversion table for RGB<->YUV conversion
 * Author: abe 2012
 * -----------------------------------------------------------------------------
 * Expected output:
 * 0.00000 0.54321 1.08642 1.62963 2.17284 2.71605 3.25926 3.80247 \
 *       0       1       1       2       2       3       3       4 \
 * 4.34568 4.88889 5.43210 5.97531 6.51852 7.06173 7.60494 8.14815 
 *       4       5       5       6       7       7       8       8 
 */

#include <stdio.h>

#define SCALEBITS (16)
#define SCALEDHALF (1L << (SCALEBITS - 1))
#define SCALEUP(x) ((x) * (1L << SCALEBITS) + SCALEDHALF)
#define SCALEDOWN(x) ((x) >> SCALEBITS)

// scaleup(x) = (2*x+1) << (scalebits-1);
// scaledown(x) = x >> scalebits;
#define SIZE (16)

int float2fixed() {
  float farr[SIZE];
  int i, iarr[SIZE];
  for(i = 0; i < SIZE; i++) {
    farr[i] = i * 0.54321;
    iarr[i] = (int)(SCALEUP(farr[i]));
  }
  for(i = 0; i < SIZE; i++)
    printf("%2.5f ", farr[i]);
  printf("\n");
  for(i = 0; i < SIZE; i++)
    printf("%7d ", SCALEDOWN(iarr[i]));
  return 0;
}

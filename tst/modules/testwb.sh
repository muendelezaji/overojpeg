#!/bin/bash
#testwb.sh
## tests the writebits program written in C over multiple values or range of values
## author: abe 2012
PROG=`basename $0`
if [ -z "$1" -o -z "$2" -o -z "$3" ]; then
echo "Runs \`writebits' for the range of values given"
echo "Usage: $PROG <min> <max> <increment>"
echo
fi

if [ -z "$1" ]; then
MIN="-4"
else
MIN="$1"
fi

if [ -z "$2" ]; then
MAX="4"
else
MAX="$2"
fi

if [ -z "$3" ]; then
INC="1"
else
INC="$3"
fi

echo "Using min: $MIN max: $MAX increment: $INC"
echo
./writebits `for (( i=$MIN; i<=$MAX; i+=$INC )); do echo -n $i" "; done`

/**
 * Prototype for the Huffman encoding procedure of writing individual bit values
 * to an output stream
 * Author: abe 2012
 * -----------------------------------------------------------------------------
 * Example output (using ./testwb.sh -31 31 3
 * value: -31	size:  5	 code: 00000	seq: -32	bits: 00000
 * value: -28	size:  5	 code: 00011	seq: -29	bits: 11000
 * value: -25	size:  5	 code: 00110	seq: -26	bits: 01100
 * value: -22	size:  5	 code: 01001	seq: -23	bits: 10010
 * value: -19	size:  5	 code: 01100	seq: -20	bits: 00110
 * value: -16	size:  5	 code: 01111	seq: -17	bits: 11110
 * value: -13	size:  4	 code: 0010	seq: -14	bits: 0100
 * value: -10	size:  4	 code: 0101	seq: -11	bits: 1010
 * value:  -7	size:  3	 code: 000	seq:  -8	bits: 000
 * value:  -4	size:  3	 code: 011	seq:  -5	bits: 110
 * value:  -1	size:  1	 code: 0	seq:  -2	bits: 0
 * value:   2	size:  2	 code: 10	seq:   2	bits: 01
 * value:   5	size:  3	 code: 101	seq:   5	bits: 101
 * value:   8	size:  4	 code: 1000	seq:   8	bits: 0001
 * value:  11	size:  4	 code: 1011	seq:  11	bits: 1101
 * value:  14	size:  4	 code: 1110	seq:  14	bits: 0111
 * value:  17	size:  5	 code: 10001	seq:  17	bits: 10001
 * value:  20	size:  5	 code: 10100	seq:  20	bits: 00101
 * value:  23	size:  5	 code: 10111	seq:  23	bits: 11101
 * value:  26	size:  5	 code: 11010	seq:  26	bits: 01011
 * value:  29	size:  5	 code: 11101	seq:  29	bits: 10111
 */

#include <stdlib.h>
#include <stdio.h>

void writebits(int seq, FILE *out) {
    int tmp = seq, i, len;
    unsigned int  nbits = 1, nbytes = 1;
    int is_neg = (seq < 0);
    printf("value: %3d\t", tmp);
    if(is_neg) {
        tmp = -tmp; // negate
        //seq = ~seq;
        seq--;
    }
    while(tmp>>=1)
        nbits++;
    printf("size: %2d\t code: ", nbits);
    for(i=nbits-1; i>=0; --i)
        printf("%c", (seq & (1<<i))?'1':'0'); // print bit representation
    if(nbits > 8)
      nbytes = 2;
    len = nbits;
    //if(is_last) {
    //    len = 8 * nbytes;
    //    for(i=nbits; i<len; ++i)
    //        seq |= 1<<i; // append '1's to complete byte
    //    printf("\trem: %d", len-nbits);
    //}
    printf("\tseq: %3d\tbits: ", seq);
    for(i=0; i<len; ++i)
        printf("%c", (seq & (1<<i))? '1':'0'); // print in order written to file
    printf("\n");
    if(out!=NULL)
        fwrite(&seq, 1, nbytes, out); //fwrite(*ptr,size,count,stream);
}

int huffwritebits(int argc, char **argv) {
    FILE *out;
    char name[16];
    int i, value;
    if(argc < 2) {
        sprintf(name, "zout%d.bin", 4095);
        writebits(4095, NULL);
        return 0;
    }
    for(i=0; i<argc-1; ++i) {
        if((value = atoi(argv[i+1])) == 0) { //sscanf(argv[i+1], "%d", &value);
            printf("Found 0, ignoring\n");
            continue;
        }
        sprintf(name, "zout%d.bin", value);
        if((out = fopen(name, "wb")) == NULL)
            continue;
        writebits(value, out);
        fclose(out);
    }
    return 0;
}


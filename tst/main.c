/**
 * @file main.c
 * @brief Runs the tests for the JPEG image encoder.
 *
 * @date 24 Jan 2012
 * @author abe
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "jpeg.h"

/**
 * By defining EXTERN_INIT, the initialisation is done here (by the caller),
 * instead of the compressor itself. This allows initialising 1 set of colour
 * conversion, quantisation and Huffman tables/routines and using them with
 * multiple images without reinitialisation.
 * Make sure to set the target JPEG quality before call to jpeg_init_compressor()
 */
//#define EXTERN_INIT

#define PROGRAM "overojpeg"
#define LENGTH 50 // Allowed length of input/output file names

#ifdef DEBUG
extern int mem_count;
#endif

unsigned char *read_bytes_from_file(char *filename);
static void set_jpeg_params(JPEG_PARAMS_obj *param, INT32 w, INT32 h, INT32 comp, \
        UINT32 quality, COLOUR_SPACE cs, SAMPLE_FORMAT sf, BOOL is_extern);

int main(int argc, char **argv) {
    int in_width, in_height, in_components, out_quality;
    char in_file[LENGTH], out_file[LENGTH];
    if(argc < 5) {
        printf("Usage: %s input_file.raw width height components <out_quality> <output_file.jpg>\n", PROGRAM);
        printf("Arguments in <> are optional (default: 95%%, output.jpg)\n");
        exit(1);
    }
    if(argc < 6) {
        out_quality = 95;
        printf("Output quality setting not given, defaulting to 95%%.\n");
    }
    else {
        out_quality = atoi(argv[5]);
    }
    if(argc < 7) {
        sprintf(out_file, "samples/out/output.jpg");
        printf("Output file name not given, writing to '%s'\n", out_file);
    }
    else {
        sscanf(argv[6], "%s", out_file); // sprintf(out_file, argv[6]);
    }
    sscanf(argv[1], "%s", in_file); // sprintf(in_file, argv[1]);
    in_width = atoi(argv[2]);
    in_height = atoi(argv[3]);
    in_components = atoi(argv[4]);
    if(!in_width || !in_height || !in_components) {
        printf("Invalid input width, height or number of components given. Program will exit.\n");
        exit(1);
    }

    int extern_init = 0;
    JPEG_ENC_obj enc; // JPEG encoder instance

#ifdef EXTERN_INIT
    // Initialise the compression process
    enc.quality = out_quality;
    jpeg_init_compressor(&enc);
    extern_init = 1;
#endif

#if 0
    /* Cannot do this pre-C99 (which is currently not supported by TI's C64x+ compiler as of v7.3.1) */
    // Current image's parameters
    JPEG_PARAMS_obj params = {
        in_width,       // width
        in_height,      // height
        in_components,  // no. of components
        out_quality,    // JPEG quality
        CS_RGB,         // colourspace (will be changed to CS_GRAY if in_components < 2)
        SF_NONE,        // sampleformat (use 0 or SF_NONE, others not implemented)
        TRUE            // external buffer, compressor will only assign a pointer not do alloc/free
    };
#else
    JPEG_PARAMS_obj params;
    set_jpeg_params(&params, in_width, in_height, in_components, out_quality, CS_RGB, SF_NONE, TRUE);
#endif

    // Uncompressed data for the image - may be read from somewhere else
    unsigned char *data = read_bytes_from_file(in_file);

    // Pass the data, parameters and encoder instance to the compressor
    jpeg_compress(data, &params, &enc, out_file, extern_init);

    if(data) {
        free(data);
    }

#ifdef EXTERN_INIT
    // Finish and clean up
    jpeg_terminate_compressor(&enc);
#endif
#ifdef DEBUG
    fprintf(stderr, "%s In main(): %d bytes were allocated but not released.\n", \
            (mem_count)? "WARNING" : "", mem_count);
#endif
    return 0;
}

static void set_jpeg_params(JPEG_PARAMS_obj *param, INT32 w, INT32 h, INT32 comp, UINT32 quality, COLOUR_SPACE cs, SAMPLE_FORMAT sf, BOOL is_extern) {
    param->width = w;
    param->height = h;
    param->components = comp;
    param->quality = quality;
    param->colourspace = cs;
    param->sampleformat = sf;
    param->extern_buffer = is_extern;
}

unsigned char *read_bytes_from_file(char *filename) {
    unsigned int size, position;
    // Open file for reading
    FILE *fptr = fopen(filename, "rb");
    if(fptr == NULL) {
        fprintf(stderr, "Failed to open file\n");
        exit(1);
    }

    // Get file size
    position = 0;
    fseek(fptr, 0, SEEK_END);
    size = ftell(fptr);
    rewind(fptr); // Return to start of file - equivalent to fseek(fptr, 0, SEEK_SET)

    // Allocate memory to read in data
    unsigned char *data = (unsigned char *)malloc(size); // allocate space
    if(data == NULL) {
        fprintf(stderr, "Failed to allocate memory\n");
        exit(1);
    }
    position = fread(data, 1, size, fptr);
    if(position != size) {
        fprintf(stderr, "Failed to read file\n");
        exit(1);
    }

    // Reset position pointer and close file
    fclose(fptr);
    return data;
}
